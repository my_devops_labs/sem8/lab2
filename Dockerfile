FROM alpine:3
EXPOSE 8000
VOLUME /app/catalog/static
RUN mkdir /app
COPY ./app /app/
RUN apk add --no-cache python3 py3-pip
RUN python3 -m pip install -r /app/requirements.txt --break-system-packages
RUN python3 -m pip install tzdata --break-system-packages
ENTRYPOINT ["python3", "/app/manage.py"]
CMD ["makemigrations"]
CMD ["migrate"]
CMD ["collectstatics","--noinput"] 
CMD ["test"]
CMD ["createsuperuser", "--noinput",  "--username=admin"]
CMD ["runserver", "0.0.0.0:8000"]
